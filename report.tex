\title{Exploiting Mobile Entities for Urban Air Quality Monitoring \\ MInf Project Plan}
\author{Dale Myers \\ 0942590}
\date{\today}

\documentclass[12pt]{article}

\usepackage{tabularx}
\usepackage{float}
\usepackage{url}
\usepackage{verbatim}
\usepackage{fixltx2e}
\usepackage{graphicx}

\begin{document}
\maketitle

\newpage

%\begin{abstract}
%\end{abstract}


\section{Purpose}\label{purpose}

Urban sensing is an increasingly important topic in the modern world. The population is increasing at a rapid pace and the infrastructure is not always adequate for the peoples needs. Urban sensing can help alleviate some of these problems. An example is monitoring the current air quality in the city to ensure that there are no health risks to the people. Without this monitoring there could be serious effects on the lives of many people. Other needs are less serious such as monitoring the quality of road surfaces or congestion at traffic lights. As time passes, more and more sensing applications evolve and it is important to make sure that these are as optimal as possible. 

Air quality is a particular interest to many people. Currently in most major cities, especially in the western world, there are many stations which monitor air quality and upload their results to a central repository of some kind. This approach gives a broad overview of the quality of the air in the immediate area of the sensors, but often fails to provide a much more broad view of the whole city. Furthermore these sensors only sample some pollutants as little as once per day. An example of this type of sensing can be seen by visiting the Scottish Air Quality website\cite{scottishairquality}. For this setup readings are only updated every hour, a rate which is far from the idea refresh rate of real time. Furthermore, with this example there are only five sites within the Edinburgh area, of which only four seem to be functional. 

In Edinburgh there is particular interest in two use cases. With both cases there is an important restriction. This restriction is that the updates be as close to real time as possible while still maintaing a low cost both economically and with respect to power usage. With this restriction in place careful attention must be paid to the design of the systems. 

With our first use case, there is a wish for the council to have a broad view of the air quality within the city. For this example I will employ a method of gathering data which will involve one or more sensor access points (SAPs) with a yet to be determined number of sensors placed on buses. These buses will drive around the city collecting data, and uploading it whenever they pass within range of a SAP.

The second case is similar but requires a different solution. There are reports of an odour problem in the Seafield area of the city which requires investigation. In order to collect data on this problem the sensor will be changed to measure odour rather than air pollutants. The method of collecting data will be to fix the sensors in predetermined locations and exploit the mobility of people and their smart phones, running a custom developed application, to ferry the data from the sensors back to a SAP, or to upload from the phone immediately. 

The issue arising with both cases is that of optimisation. We must look at making the deployment cost as little as possible while also being as efficient as possible. With our first case we must determine certain conditions such as whether it would be more efficient to pass data between the buses as they pass by each other at the cost of increased power usage, or to wait until a SAP is in range which increases the latency. Similarly with the second case we must pay close attention to the battery usage of the application on the users smart phone. We cannot scan constantly for sensors as this would diminish the remaining battery life greatly and so we must compromise. 

The rest of the paper is organised as follows: in section \ref{background}, we present some background information what air quality is as well as the different methods of acquiring data from sensors. We will also look briefly at some other similar projects and see how they relate to this proposal. In section \ref{methodology}, the methodology of how I will perform these experiments and gather data is explained along with potential issues. Section \ref{evaluation} describes how the project will be evaluated and section \ref{preliminary} is a review of some preliminary work which has been done. 

\section{Background and Related Work}\label{background}

\subsection{What is air quality?}\label{what is air quality}

A definition of air quality is a difficult problem which has not been solved. Many institutions measure and record what they refer to as ``air quality'', however the only thing these measurements have in common is that no two are the same. In order to research the area of ``air quality'' further we must have a concrete definition of what air quality is. Current definitions give a simple definition of ``air quality'' as being antonymous to that of air pollution in that higher air quality is the equivalent to low air pollution\cite{bcaq}. This implies that not only should the metrics used for defining air quality be considered, but also those used to define air pollution.

Looking at statements and measurements by various authorities around the world \cite{epapollutants}\cite{airqualityobjectives}\cite{cleanairnavigation}\cite{naaqs}\cite{whoguidelines}, we can build a list of common pollutants which are measured:


\begin{itemize}
\item Sulphur dioxide
\item Nitrogen dioxide
\item PM10 
\item PM2.5
\end{itemize}

These chemicals can form the basis of pollutants which in turn can help define ``air quality''. By looking at other sources\cite{meaningsofenvironmentalterms},  proposed definition of ``air quality'' is as follows:

\begin{quote}
``The current measurements of the concentrations of certain pollutants in the air relative to the requirements of one or more biotic species or to any human need or purpose.''
\end{quote}

\subsection{Sensing and Data Collection Paradigms}\label{sensing paradigms}
    
While there are many different types of sensing, there are few key collection paradigms. Here we discuss some of these compare and contrast them.

\subsubsection{Meshed Sensor Network}\label{meshed sensor network}

\begin{figure}[H]
	\begin{center}
		\includegraphics[scale=0.5]{./images/MobileMesh.png}
		\caption{Diagram of a meshed sensor network. Image from \cite{datacollectionsurvey}}
	\end{center}
\end{figure}


Meshed sensor networks are the simplest paradigm to understand. Each node in the network is in range of at least one other node with the graph being connected. When a node takes a reading it simply routes it through the network back to the SAP where it is stored for later connection or uploaded to the internet. Different networks require different physical components with short range networks using Bluetooth for their connection, but larger networks using protocols such as ZigBee\cite{btvszigbee}. In these networks the nodes are usually static, but given a high enough node density they may be in motion and yet remain meshed. 

\subsubsection{Static SAPs with Mobile Sensors}\label{static sap mobile sensors}

\begin{figure}[H]
	\begin{center}
		\includegraphics[scale=0.4]{./images/StaticSAPMobileSensors.png}
		\caption{Diagram of a sensor network with mobile sensors and a static SAP. Image from \cite{datacollectionsurvey}}
	\end{center}
\end{figure}


This model builds on the last in that the nodes are in motion constantly, but do not always have to be in range of other sensors. Links between nodes and the SAPs are dynamic and can break and form relatively easily. As a node comes within range of a SAP it will form a connection and transfer whatever data it has, or as much as possible depending on the time limit, before terminating the connection as it moves out of range of the SAP. 


\begin{figure}[H]
	\begin{center}
		\includegraphics[width=\linewidth]{./images/CommunicationRange.png}
		\caption{Diagram of the communications range of a SAP and a node. Image from \cite{datacollectionsurvey}}
	\end{center}
\end{figure}

\subsubsection{Mobile SAPs with Static Sensors}\label{mobile sap static sensors}

\begin{figure}[H]
	\begin{center}
		\includegraphics[scale=0.5]{./images/StaticSensorsMobileSAP.png}
		\caption{Diagram of static sensors with mobile SAPs. Image from \cite{datacollectionsurvey}}
	\end{center}
\end{figure}


The inverse of the previous example is to have static sensors placed with the access points moving around collecting the data from them. These SAPs can retain the data and upload to a remote server at a later time, or upload immediately given some other form of connection. 

\subsubsection{Static Sensors with Data Mules}\label{static sensors data mules}

\begin{figure}[H]
	\begin{center}
		\includegraphics[scale=0.5]{./images/StaticSensorsDataMule.png}
		\caption{Diagram of static sensors with data mules\cite{datacollectionsurvey}}
	\end{center}
\end{figure}

The final paradigm is to have static sensors and static SAPs out of range of each other and form the link using what is known as a \emph{data mule}\cite{datamulesthreeteir}\cite{multimules}. There is some form of moving object which is capable of collecting data from the sensors and transporting it to the SAP. This is the most uncommon paradigm, but appears in unexpected places. An example use of this is internet access in remote villages where emails would be sent and received while there is a bus in the village, but the connection is broken until the bus returns. 

\subsubsection{Comparison}\label{paradigm comparison}

\begin{table}[H]
    \begin{tabularx}{\linewidth}{ | X | X | X | X | X | }
	\hline
		 & Meshed & Static SAP Mobile Sensors & Mobile SAP Static Sensors & Static Sensors with Data Mules \\ \hline
		Multihop Network & Yes & Possible & No & Possible \\ \hline
		Mobility & No & Sensors & SAP & People, Buses, etc. \\ \hline
		Sensors & Static & Dynamic & Static & Static \\ \hline
		Latency & Low & High & High & High \\ \hline
		Packet Loss & Low & Low & Low & High \\ \hline
    \end{tabularx}
\end{table}



\subsection{City Sensing Projects}\label{city sensing projects}

\textbf{BikeNet} - BikeNet\cite{bikenet} is a mobile sensing system for cyclists. It helps cyclists monitor many aspects of their activity such as speed, distance travelled and calories burned. It achieves this by using an array of hardware on a bike which can record various metrics. Once it has this data it uses the \emph{Static SAP with Mobile Sensor} paradigm to store the data centrally. As the rider moves past a SAP the data is uploaded automatically. It also has the option to have a mobile SAP with them. This would be connected to a mobile phone and utilise its GSM connectivity to connect to the world wide web, uploading the data in almost real time. 
\\
\textbf{CitySense} - CitySense\cite{citysense} is a more abstract example than other systems. Its main purpose is to provide an infrastructure for other applications to utilise in distributed sensing efforts. By utilising around 100 embedded systems running a Linux distribution with dual 801.11a/b/g radios they have formed a \emph{meshed network} across the city of Cambridge, Massachusetts. By allowing the nodes to be programmed directly by users they allow their hardware to be used for a myriad of possible projects. 
\\
\textbf{CarTel} - CarTel\cite{cartel} is an implementation of a hybrid of our different models. It uses what is referred to as ``opportunistic wireless connectivity'', which simply means that it uses any available method of communication to relay data back to an access point. Methods include using other nodes, USB flash drives and mobile phones. This method of data tranfer was developed as part of CafNet\cite{cafnet}. It is most similar to the \emph{Static Sensors with Data Mules} model with the difference that it is much more aggressive in acquiring a route to the central server. Using this approach CarTel can collect and route information from cars back to the SAP, referred to in the paper as a \emph{portal}. 

\section{Methodology}\label{methodology}

The aim for each implementation is to take readings from various sensors and store them in a central server. These readings may be air quality readings for the case where the city of Edinburgh council are looking for a broad overview of air quality within the city, or odour readings in the case of the Seafield problem. I plan to look at two of the models in detail by implementing them. These models are the \emph{Static SAPs with Mobile Sensors} model and the \emph{Static Sensors with Data Mules} model. Both will be discussed in detail, however there are problems common to both. 

\subsection{SAP}\label{sap}

In order to implement any of these models we must have a SAP for recording the data. The SAP for all models will be directly connected to the internet so that whenever a reading is returned it can be immediately uploaded to the central server. The hardware will be a small embedded system, such as the \emph{Gateworks Avila GW2348-4}\cite{aviladatasheet} network processor board. A wireless receiver will be attached to the board in order to listen for readings. Board such as the Avila can run the linux operating system and so development of software can be greatly accelerated by using a programming language such as Python. The other end of the system is the server side component. This will be developed using PHP and MySQL to record data and store it in an efficient format. 

\subsection{Sensor Platform}\label{sensor platform}

A sensor platform is also required for these experiements. In 2012 a student created a device known as the \emph{EnviSensor}\cite{envisensor}. This device is based on an Arduino Uno and records a few metrics about the current air quality. These are temperature, humitidy and an index representing the air quality. This index is an integer value which takes into account a wide range of pollutants, giving an overall view of the currently present pollutants. Using a Bluetooth sheild it sends the data to an application which runs on Android devices. It is this sensor which will be the basis for the sensor platform I will use. 

The first change which needs to be made is that the bluetooth shield is removed and replaced with a connection to an Avila board. This connection will use the UART protocol and due to the current design of the sensor no changes will need to be made to the code running on the sensor to accommodate this change. The Avila board will receive these readings and store them locally in a database. Depending on the experiement it will upload the data to a SAP when necessary. 

A further requirement on the sensor platform is for the Seafield odour problem experiment. In this case we do not wish to measure the air quality and instead wish to focus on olfactory readings. The air quality sensor will need to be replaced with a new olfactory sensor and the code changed to take this into account. 

With both the sensor platform and the SAP, paying attention to radio power is also an important consideration. If we were to boost the power of the radios we would be able to remain in contact for longer as the two move past each other in the models with mobility. A longer contact time would allow more data to be sent\cite{vehicleinternetaccess} at the cost of increasing the power consumption. 

\subsection{Static SAPs with Mobile Sensors}\label{static sap mobile sensors methodology}

With thanks to Dr Mark Wright, Lothian Buses have agreed to place some sensors on their buses. Starting off with a small trial of just two or three buses, these buses would travel around their routes and record data. The SAP would be placed at the bus depot or some other secure location or locations the buses all visit. Assuming the initial trials are successful, the operation would be expanded to hopefully around ten sensors. Due to the broad area that the buses cover\cite{lothianbusroutes}, these sensors will measure air quality as we will get the most comprehensive picture of the air quality within the city. There are also few buses which go through the Seafield area and so we may not get accurate enough results to be of use. 

With the current model we need to make decisions about certain parameters. A key parameter being the number of SAPs. The simplest model has a single SAP. This model would be fairly simple to plan for. With information about the bus routes we could even store information about the bus time tables to make sure that the sensor is only looking for the SAP when it likely to be near by, thus saving power. With more SAPs the situation gets slightly more complicated. We would have to listen for a signal for longer periods of time, which would increase the power consumption. The advantage would be a greatly reduced latency period between taking the reading and it being available on the central server. 

A further optimisation would be to allow the buses to communicate with each other to ensure that data is routed back as quickly as possible. In order to do this most efficiently we would need to code each sensor with the bus routes as well as which bus the sensor is on in order to detect what the quickest route back to the base station would be. This approach has the disadvantage in that it greatly increases the complexity of the system. With a greater complexity we are more likely to encounter problems which may be extremely difficult to solve. We must look at the trade off between the latency of the readings and the complexity of the system. 


\subsection{Static Sensors with Data Mules}\label{static sensors data mules methodology}

The exact implementation of this paradigm has not yet been confirmed, however there is a likely route which will be followed. This model will be used in the odour problem case in the Seafield area. In this area we have very few buses going through the area and so we must exploit a different type of mobility in the area to record our readings. The best choice for this is the public. Many people now have a smart phone of some kind and it is this peice of technology which we will leverage to our advantage. 

The sensors will be placed around the Seafield area, most likely on lampposts to alleviate the problem of power consumption, but other street furniture being a possibility. As people move through the area with a custom application running on their mobile phone, the phone will scan for sensors and download data from these sensors. The phones will temporarily store this data before passing it on to a SAP, or more likely uploading directly to the internet when they have a suitable form of connection such as wifi. 

The most important aspect of this deployment is the application running on the users phones. This application can take one of two forms. It may be a standalone application whose sole purpose is to collect this data, or it may be part of another application, potentially provided by the council. In either case the application will have to be carefully designed and implemented. The application needs to scan for sensors and so needs to have access to the phones Bluetooth radio. It also needs network capabilities in order to be able to upload any data it obtains. We need to store data on the phone temporarily, but do not have unlimited buffer space on the users phones so this will need to be optimised if possible. 

With an application on the users phones there are certain considerations we must make. The most important is that of power usage. Smart phones have very limited battery life due to having to power some rather powerful components. We want to reduce our power usage if possible, while still being able to listen for sensors so that we do not pass right by without gathering any data. The other end is that of uploading the data. We have similar restrictions with power. We could upload immediately, using either wifi or, with the users permission, cellular connections, which would give us the lowest latency, or we can store a certain amount before uploading in order to reduce the power usage. 

There is a final addition to this model which is not as essential for the \emph{Static SAP with Mobile Sensors} model. Humans are not as reliable as buses and as such we need to have some kind of acknowledgement method to ensure that we do not lose any data. 


\section{Evaluation}\label{evaluation}

Each of our paradigms needs to be evaluated in terms of power consumption for the sensors and the SAPs. By connecting some sort of power monitoring hardware to the sensors, we can return power readings with the sensor readings a build up an model of how our usage affects power consumption. We also need to monitor the power consumption of the users phones in the model using the users phones as data mules. 

The second metric which we need to carefully record is that of the latency between the reading being taken and when it is available on the central server. This is simply measured by recording the time the reading is taken, and timestamping each hop of the journey back to the central server. By doing this we can map out any potential bottle necks as well as find out which sensors are not being visited often enough by either buses or people depending on the implementation. 

A third metric which can be very important is the complexity of the system. Looking back at the \emph{Static SAP with Mobile Sensor} model we can add in the features such as dynamic routing between buses. This would add a huge layer of complexity to the model due to having to code in routing algorithms. Complexity is something which is more difficult to measure, but a possible way of doing it is by looking at the hours of development and testing put into the system. 

With regards to the smart phone application in the second model, assuming we have a standalone application, it would be interesting to get user opinions and evaluations of the software. This could include things such as ease of use, whether they believed their battery performance was degraded and whether they believed the application was worth the effort. 

\section{Preliminary Work}\label{preliminary}

During the universities winter break I used the time to test some concepts of data storage and management as well as start working on a reading visualisation tool. In order to test this I needed to gather data. Using the EnviSensor created by Eric Shane Staples, I took the sensor around the city to generate readings. Roughly half of the data points were gathered by simply walking around but the other half was generated by having the sensor on a vehicle. One set was generated by attaching the sensor to a car and driving around the city. The other two sets were measured more consistently by taking the sensor on an open top tour bus. I completed the entire route and repeated the experiment on a second day to ensure accuracy. Using the data points and open street map\cite{openheatmap} I have generated two heat maps of air pollution within the city.

\begin{figure}[H]
	\begin{center}
		\includegraphics[width=\linewidth]{./images/Trip1.png}
		\caption{Heatmap of the first trip}
	\end{center}
\end{figure}

\begin{figure}[H]
	\begin{center}
		\includegraphics[width=\linewidth]{./images/Trip2.png}
		\caption{Heatmap of the second trip}
	\end{center}
\end{figure}


These initial tests were very successful and are a stepping stone to the next phase of the project which starts with developing a full sensing platform. Currently the sensor connects to an Android running device using a Bluetooth connection. For the full platform the Android device will be replaced with an Avila board running OpenWrt or similar and will connect to the sensor using a serial connection. The current sensor is for measuring air quality and as such will work for the use case where we measure air quality across the city, but this will need to be replaced with some form of odour sensor for the Seafield odour problem case. 

\section{Work Plan}\label{plan}

\begin{figure}[H]
	\begin{center}
		\includegraphics[width=\linewidth]{./images/GanttChart.png}
		\caption{Work Plan}
	\end{center}
\end{figure}

\newpage


\nocite{vehicalinternetaccess}
\nocite{manetmessaging}
\nocite{surveyofmobilephonesensing}
\nocite{capacitydelaymobility}
\nocite{meshedrouting}
\nocite{opportunisticsensingsecurity}
\nocite{relaysbasestationsmeshes}
\nocite{cachingstrategies}


\bibliographystyle{unsrt}
\bibliography{references}


\end{document}

